
I) find_empirical_kappas.pl Perl script 

	Usage: perl find_empirical_kappas.pl network_file.txt > kappas_file.txt

	Description: This script computes the empirical kappas (average agent degrees per slot) in a given temporal network stored in network_file.
   	The format of the network_file should be as in the files under the DATA folder (first column slot number, next two columns the IDs of the interacting nodes).
    The output is written in kappas_file.txt.

II) find_links_per_slot.pl Perl script 

	Usage: perl find_links_per_slot.pl network_file.txt > links_file.txt 
	
	Description: This script finds the number of links in each slot in a given temporal network stored in network_file.
	The format of the network_file should be as in the files under the DATA folder.
	The output is written in links_file.txt.
	
III) properties_plotter.py Python script

	Usage: python properties_plotter.py -o plot_name -f input_directory -l legend_synthetic_networks
	
	This command will output a pdf file named plot_name consisting of 8 subplots; each corresponding to a different proximity network property. 
	The input_directory must contain any number of real and synthetic proximity networks. The real networks are provided in the DATA directory of this repository while synthetic networks are any other network files of the same format.
	Each subplot shows the results for each real network with different marker styles and a black line for the average result of all synthetic networks. 
	The argument legend_synthetic_networks indicates the text to describe the average result of the synthetic networks in the subplots.

	The 8 properties are:
	1) Contact duration distribution(subplot a)
	2) Inter-contact duration distribution (subplot b)
	3) Weight distribution (subplot c)
	4) Strength distribution (subplot d)
	5) Component size distribution (subplot e)
	6) Node degree VS node strength (subplot f)
	7) Group size VS group duration (subplot g)
	8) Number of interactions VS participation in recurrent components (subplot h)

Requirements:
	python 2.7
	
	libraries:
		matploblib
		numpy
		
	OS:
		Linux
		Mac
	

--------------------------------------------------------------------------------------------------
Authors: Fragkiskos Papadopoulos & Marco Antonio Rodriguez Flores
Publication: "Latent geometry and dynamics of proximity networks"
https://arxiv.org/abs/1907.00073 
