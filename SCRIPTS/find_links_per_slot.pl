#!/usr/bin/perl
  
use strict;

my $infile0 = $ARGV[0];
open (INPUT0, "$infile0") or die "Can't open file $infile0\n";
my %num = ();
my %seen=();
while(my $inline = <INPUT0>) {
    my @line = split(' ', $inline);
    if(!defined $seen{"$line[0] $line[1] $line[2]"}) { 
       $num{$line[0]}++;
       $seen{"$line[0] $line[1] $line[2]"} = 1;
       $seen{"$line[0] $line[2] $line[1]"} = 1;
    }
}
close(INPUT0);

for my $n (sort {$a<=>$b} keys %num){
    printf "$n $num{$n}\n";
}
