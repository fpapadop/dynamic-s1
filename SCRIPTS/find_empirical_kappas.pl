#!/usr/bin/perl
  
use strict;

my $infile0 = $ARGV[0];
my %deg2node = ();
open (INPUT0, "$infile0") or die "Can't open file $infile0\n";

my %slots = ();
my $max_slot = 0;
my %seen=();

while(my $inline = <INPUT0>) {

    my @line = split(' ', $inline);

    if(!defined $seen{"$line[0] $line[1] $line[2]"}) {

       $deg2node{$line[1]}++;
       $deg2node{$line[2]}++;

       $slots{$line[0]} = 1;

       if($line[0] > $max_slot) {
          $max_slot = $line[0];
       }

       $seen{"$line[0] $line[1] $line[2]"} = 1;
       $seen{"$line[0] $line[2] $line[1]"} = 1;
    }
}
close(INPUT0);

for my $n (keys %deg2node) {

    my $bar_k = $deg2node{$n}/$max_slot;
    printf "$bar_k\n";

}
