# dynamic-S1 code, network datasets and scripts

This repository contains the code implementing the dynamic S1 model and the datasets from the paper: "Latent geometry and dynamics of proximity networks", F. Papadopoulos and M. A. Rodríguez Flores, Physical Review E, Vol. 100, Issue 5, November 2019 (https://arxiv.org/abs/1907.00073).
Additionally, we include two perl scripts for computing the two files that the dynamic-S1 program takes as input and a python script that computes and plots several temporal network properties.

## dynamic-S1 code

This is the implementation in C++ of the dynamic-S1 model. 

+ **Source file: ** dynamic_S1.cpp
			
+ **Compilation: **

	** 1. Requirements: **	The program can be compiled in Linux or Mac

	** 2. Compilation command: **	
	
		g++ dynamic_S1.cpp -std=c++11
			
+ **Running the program: **

	** 1. Inputs: ** The program requires the following input files and parameters to generate a synthetic network.
	
	#####1.1 A file containing the expected degree per slot of each agent in the network (kappa). 
		
		To compute empirical kappas from a given network stored in say network_file, one can use the perl script "find_empirical_kappas.pl" included in this repository.
			
		To run this script use the following command to produce the output in kappas_file: perl find_empirical_kappas.pl network_file > kappas_file 
			
		The format of the network_file should be as in the files under the DATA folder (first column slot number, next two columns the IDs of the interacting nodes).

	######The kappas_file has one column containing the average degree per slot (kappa) of each node.
		
		example: 	0.039652394106814
					0.0640538674033149
					0.0256100368324125
					0.0012085635359116
					0.0143301104972376
					0.0488029465930018
					...
		
	#####1.2 A file containing the number of links in each slot of the network. 
		
		From this file the code computes the average snapshot degree in each slot (2L/N).
		
		To compute the empirical number of links in each slot of a given network stored in network_file, one can use the perl script "find_links_per_slot.pl" included in this repository.
			
		To run this script use the following command to produce the output in links_file: perl find_links_per_slot.pl network_file > links_file 
				
	######The links_file has two columns: the first is the time slot number and the second is the number of links in the time slot.
	
		example:	1 1
					2 1
					19 1
					20 1
					22 1
					23 1
					...
			
	#####1.3 Temperature parameter of the dynamic-S1 model. It can take values in (0, 1). See the paper for details.
		
	#####1.4 Number of slots to simulate.
			
	** 2. Example execution: ** 
	
	Using the Primary School network included in this repository, a full execution of the program to generate a synthetic counterpart is as follows:
	
		1. Get the empirical kappas of the network:
			perl find_empirical_kappas.pl remapped_edgelist_Primary.txt > kappas_Primary.txt 
		2. Get the empirical number of links in each slot of the network:
			perl find_links_per_slot.pl remapped_edgelist_Primary.txt > links_per_slot_Primary.txt 
		3. Run the dynamic-S1 program:
			./a.out kappas_Primary.txt links_per_slot_Primary.txt 0.72 5846 > synthetic.txt
			
			where 0.72 is the temperature and 5846 is the number of slots for this network. See the paper for the temperature and number of slots of the other networks included in this repository.
	
	This command will produce a temporal network stored in synthetic.txt
	
	** 3. Output: ** The output file (synthetic.txt) contains 3 columns.

	The first column is the time slot and the next two columns are the IDs of the connected/interacting nodes.
		
		example: 1 2 5 (In slot 1 nodes 2 and 5 are connected).
		
##Datasets

 The datasets as used in the paper are provided in the folder "DATA"

 For more information please check the README file inside the folder.

##Scripts
 
 The scripts included in the folder "SCRIPTS" are the following:
 
	1) "find_empirical_kappas.pl" (Perl)
	
	2) "find_links_per_slot.pl" (Perl)
	
	3) "properties_plotter.py" (Python)
		
		computes and plots different temporal network properties
		
 Please check the README file inside the scripts folder for instructions and further details.

***
Authors: Fragkiskos Papadopoulos (email:f.papadopoulos@cut.ac.cy) and Marco Antonio Rodriguez Flores (email: mj.rodriguezflores@edu.cut.ac.cy).

Publication: "Latent geometry and dynamics of proximity networks", F. Papadopoulos and M. A. Rodríguez Flores, Physical Review E, Vol. 100, Issue 5, November 2019
(https://arxiv.org/abs/1907.00073).