#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

class Node {

        private:
        int name;
        double theta, kappa;

        public:
        Node(int id) {name = id;}
        
        int getName() {return name;}

        void setAngle(double angle) {theta = angle;}
        double getAngle() {return theta;}

        void setKappa(double k) {kappa = k;}
        double getKappa() {return kappa;}
};

class Graph {

        private:
        vector<Node*> nodeList;

        public:
        void addNewNode(Node *nNode) { nodeList.push_back(nNode); }
        vector<Node*> getNodeList() { return nodeList; }
};

double Random() {
        double r = ((double)rand()/(double)RAND_MAX);
        return r;
}

int main(int argc, char *argv[]) {

        srand((unsigned)time(NULL));
    
        Graph *G = new Graph();
        Node *u, *v;
        double bar_kappa = 0;
        int i = 1;
	
		float T;
		int tot_slots;
		vector<double> numLinks;
		
		//Argument Validations
		if (argc != 5){
			printf("Error: the program requires 4 arguments:\n");
			printf("\tInput file: Average degree per slot for each node\n");
			printf("\tInput file: Number of links per slot\n");
			printf("\tTemperature\n");
			printf("\tNumber of slots\n");
			exit(1);
		}
		else{
			T = atof(argv[3]);
			if(T <= 0 || T >= 1){
				printf("Error: temperature (T) parameter must be in the range 0 < T < 1\n");
				exit(2);
			}
			tot_slots = atoi(argv[4]);
			if (tot_slots <= 0){
				printf("Error: number of slots must be larger than 0\n");
				exit(3);
			}
		}
		
		numLinks.resize(tot_slots);
	
        //Read the input a containing the kappas.
        ifstream str(argv[1]);

        if (str.is_open()) {

            string line;
            double kappa;

            while (getline(str,line)) {

                 istringstream ss(line);
                 string v1;
                 ss >> v1;

                 char *p1;
                 kappa = strtold(v1.c_str(), &p1);

                 //Create the nodes and assign to them latent variables (kappa, theta).
                 u = new Node(i);
                 G->addNewNode(u);
                 u->setAngle(2*M_PI*Random());
                 u->setKappa(kappa);
                 bar_kappa = bar_kappa+kappa; 
                 i++;
            }

        } else {
           cout<<"Error: could not open input file\n";
           exit(-1);
        }
        str.close();

        //Read the input file containing the empirical number of links per slot.
        ifstream str2(argv[2]);
        if (str2.is_open()) {
            string line;
            double num;
            int s;
            while (getline(str2,line)) {
                 istringstream ss(line);
                 string v1, v2;
                 ss >> v1;
                 ss >> v2;
                 char *p1;
                 s = strtold(v1.c_str(), &p1);
                 num = strtold(v2.c_str(), &p1);
                 s--;
                 numLinks[s] = num;
            }
        } else {
           cout<<"Error: could not open input file\n";
           exit(-1);
        }
        str2.close();

        i--;
        //Compute the average of kappa.
        bar_kappa = bar_kappa/i;

        //Radius of the circle in the model.
        double R = i/(2*M_PI);

        vector<Node*> nodes = G->getNodeList();
        
        for(int s = 0; s < tot_slots; s++) {

            //Target average degree of the snapshot in the current slot.
            double bar_k = 2*numLinks[s]/i;
        
            if(bar_k == 0) continue;  //Empty slot.

            //mu parameter.
            double mu = bar_k*sin(T*M_PI)/(2*M_PI*T*pow(bar_kappa,2));

            for(int i = 0; i < nodes.size() ; i++) {

                for(int j = i+1; j < nodes.size() ; j++) {

                      u = nodes[i];      
                      v = nodes[j];      

                      double dtheta = fabs(v->getAngle()-u->getAngle());
                      if(dtheta > M_PI) {
                         dtheta = 2*M_PI-dtheta;
                      }
  
                      double kappa_u = u->getKappa();
                      double kappa_v = v->getKappa();

                      double chi = R*dtheta/(mu*kappa_u*kappa_v);
                      double p_uv = 1.0/(1+pow(chi, 1.0/T));
 
                      if(Random() <= p_uv) {
                          fflush(stdout);
                          cout<< s+1 <<" "<<u->getName()<<" "<<v->getName()<<"\n";
                      }
                 }
          }//Slot finishes
     }
}
