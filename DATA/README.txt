
Processed real-world datasets from www.sociopatterns.com used in "Latent geometry and dynamics of proximity networks", F. Papadopoulos, M. A. Rodriguez Flores (https://arxiv.org/abs/1907.00073)

	1) Hospital Ward in Lyon. Reference: P. Vanhems et al., Estimating Potential Infection Transmission Routes in Hospital Wards Using Wearable Proximity Sensors, PLoS ONE 8(9): e73970 (2013). URL: http://www.sociopatterns.org/datasets/hospital-ward-dynamic-contact-network/

	2) Primary School in Lyon. Reference: High-Resolution Measurements of Face-to-Face Contact Patterns in a Primary School, PLOS ONE 6(8): e23176 (2011). URL: http://www.sociopatterns.org/datasets/primary-school-temporal-network-data/

	3) High School in Marseilles. Reference: R. Mastrandrea, J. Fournet, A. Barrat,
Contact patterns in a high school: a comparison between data collected using wearable sensors, contact diaries and friendship surveys. PLoS ONE 10(9): e0136497 (2015) URL: http://www.sociopatterns.org/datasets/high-school-contact-and-friendship-networks/

	4) Hypertext 2009 Conference in Turin. Reference: L. Isella et al.,  What's in a crowd? Analysis of face-to-face behavioral networks, Journal of Theoretical Biology 271, 166 (2011). URL: http://www.sociopatterns.org/datasets/hypertext-2009-dynamic-contact-network/

The datasets have been processed in order to have the same timestamp and node ID format.

I) Dataset file names

	1) The Hospital dataset file name is "remapped_edgelist_Hospital.txt"
	2) The Primary School dataset file name is "remapped_edgelist_Primary.txt"
	3) The High School dataset file name is "remapped_edgelist_HS2013.txt"
	4) The Conference dataset file name is "remapped_edgelist_HyT2009.txt"

II) Format of the dataset files

	3 Columns:	time_slot node1_ID node2_ID
	Example:	1 3 4	(At time slot 1, Node 3 is interacting with Node 4).

III) Remapping of time slots:
	
	The time slots of the datasets have been renumbered sequentially from 1 until the final slot recorded in each dataset.

IV) Node IDs:
	
	The node IDs of the datasets have been renumbered sequentially from 1 until the the total number of nodes recorded in each dataset.
